package exercise1.nus.com.exercise1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by tranthanhhunganh on 3/28/16.
 */
public class Fragment2 extends Fragment {

    ViewGroup root;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment2, null);
        return root;
    }
    void setMessage(String msg){
        TextView txt=(TextView)root.findViewById(R.id.lbResult);
        txt.setText(msg);
    }

}
