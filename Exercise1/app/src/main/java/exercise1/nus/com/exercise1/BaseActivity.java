package exercise1.nus.com.exercise1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by tranthanhhunganh on 3/28/16.
 */

public class BaseActivity extends AppCompatActivity{

    protected Activity getActivityContext(){

        return this;
    }
}
