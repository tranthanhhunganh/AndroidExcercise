package exercise1.nus.com.exercise1;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.content.Intent;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn1 = (Button)findViewById(R.id.btnView1);
        Button btn2 = (Button)findViewById(R.id.btnView2);
        Button btn3 = (Button)findViewById(R.id.btnView3);
        Button btn4 = (Button)findViewById(R.id.btnView4);

        assert btn1 != null;
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivityContext(), Activity1.class));
            }
        });
        assert btn2 != null;
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivityContext(), Activity2.class));
            }
        });
        assert btn3 != null;
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivityContext(), Activity3.class));
            }
        });

        assert btn4 != null;
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivityContext(), Activity4.class));
            }
        });
    }
}
