package exercise1.nus.com.exercise1;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Activity1 extends AppCompatActivity implements iFragment1Delegate  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment1 fragment1 = new Fragment1();
        fragmentTransaction.replace(R.id.frl1, fragment1);

        Fragment2 fragment2 = new Fragment2();
        fragmentTransaction.replace(R.id.frl2, fragment2);
        fragmentTransaction.commit();
    }

    @Override
    public void onButtonPressed(String msg) {

        Fragment2 Obj=(Fragment2) getSupportFragmentManager().findFragmentById(R.id.frl2);
        Obj.setMessage(msg);
    }
}
