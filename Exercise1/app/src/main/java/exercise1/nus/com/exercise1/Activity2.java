package exercise1.nus.com.exercise1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Activity2 extends AppCompatActivity {

    TextView tvCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Button btnStart = (Button) findViewById(R.id.btnStart);
        Button btnStop = (Button) findViewById(R.id.btnStop);
        Button btnFinish = (Button) findViewById(R.id.btnFinish);
        tvCounter = (TextView) findViewById(R.id.lbCounter);

        BroadcastReceiver updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int a = intent.getIntExtra("counter",0);
                if(intent.hasExtra("counter") && intent.getSerializableExtra("counter") instanceof Integer){

                    tvCounter.setText("Counter: " + String.valueOf(a));
                }
            }
        };
        registerReceiver(updateReceiver, new IntentFilter("abc"));

        assert btnStart != null;
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startService(new Intent(getBaseContext(), CounterService.class));
            }
        });

        assert btnStop != null;
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(new Intent(getBaseContext(), CounterService.class));
            }
        });

        assert  btnFinish != null;
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(new Intent(getBaseContext(), CounterService.class));
                finish();
            }
        });
    }
}
