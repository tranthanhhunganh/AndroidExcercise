package exercise1.nus.com.exercise1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity4 extends AppCompatActivity {

    private Intent serviceIntent;
    private ResponseReceiver receiver = new ResponseReceiver();
    TextView tvCounter;

    public class ResponseReceiver extends BroadcastReceiver {

        // on broadcast received
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(MyIntentService.ACTION)) {
                int value = intent.getIntExtra("counter", 0);
                if(intent.hasExtra("counter") && intent.getSerializableExtra("counter") instanceof Integer){

                    tvCounter.setText("Counter: " + String.valueOf(value));
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Button btnStart = (Button) findViewById(R.id.btnStart);
        Button btnStop = (Button) findViewById(R.id.btnStop);
        Button btnFinish = (Button) findViewById(R.id.btnFinish);
        tvCounter = (TextView) findViewById(R.id.lbCounter);

        assert btnStart != null;
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serviceIntent = new Intent(new Intent(getBaseContext(), MyIntentService.class));
                startService(serviceIntent);
            }
        });

        assert btnStop != null;
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (serviceIntent != null) {

                    stopService(serviceIntent);
                }
            }
        });

        assert btnFinish != null;
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (serviceIntent != null) {

                    stopService(serviceIntent);
                }
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(MyIntentService.ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }
}
