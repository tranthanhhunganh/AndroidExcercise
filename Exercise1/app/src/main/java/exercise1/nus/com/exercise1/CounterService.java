package exercise1.nus.com.exercise1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Timer;
import java.util.Date;
import java.util.TimerTask;

public class CounterService extends Service {
    public CounterService() {
    }

    int counter = 0;
    private Timer timer;
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void increaseCounter() {

        // creating timer task, timer
        timer = new Timer();


        // scheduling the task
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                timerRun();
            }
        },0,1000);
    }

    private void timerRun() {

        counter += 1;
        Intent i = new Intent();
        i.setAction("abc");
        i.putExtra("counter", counter);
        this.sendBroadcast(i);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
       this.increaseCounter();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        timer.cancel();
        timer.purge();
    }
}
