package exercise1.nus.com.exercise1;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.lang.ref.WeakReference;

/**
 * Created by tranthanhhunganh on 3/28/16.
 */
public class Fragment1 extends Fragment {

    private iFragment1Delegate delegate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment1, null);
        init(root);
        return root;
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        try {

            delegate = (iFragment1Delegate) getActivity();
        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString() + " must implement onButtonPressed");
        }
    }
    void init(final ViewGroup root){

        Button but=(Button)root.findViewById(R.id.btnSubmit);
        but.setOnClickListener(new View.OnClickListener() {

        EditText tf=(EditText)root.findViewById(R.id.tfInput);
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                delegate.onButtonPressed(tf.getText().toString());
            }
        });
    }
}
