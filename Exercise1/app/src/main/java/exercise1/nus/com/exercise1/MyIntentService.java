package exercise1.nus.com.exercise1;

import android.app.IntentService;
import android.content.Intent;
import android.os.*;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class MyIntentService extends IntentService {

    public static final String ACTION ="MY_ACTION";
    public Boolean FLAG = true;
    private int counter = 0;

    public MyIntentService() {

        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MyIntentService.ACTION);

        while (this.FLAG) {

            counter += 1;
            broadcastIntent.putExtra("counter", counter);
            sendBroadcast(broadcastIntent);
            SystemClock.sleep(1000);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.FLAG = true;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        this.FLAG = false;
    }
}
